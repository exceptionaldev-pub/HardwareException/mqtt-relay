// This example uses an Arduino Uno together with
// an Ethernet Shield to connect to shiftr.io.
//
// You can check on your device after a successful
// connection here: https://shiftr.io/try.
//
// by Joël Gähwiler
// https://github.com/256dpi/arduino-mqtt

#include <Ethernet.h>
#include <MQTT.h>

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};
byte ip[] = {192, 168, 1, 177};  // <- change to match your network

EthernetClient net;
MQTTClient client;

unsigned long lastMillis = 0;
int pinOCD = 2;
int pinL1 = 3;
int pinL2 = 4;
int pinL3 = 5;
int pinL4 = 6;
int pinL5 = 7;
int pinL6 = 8;
int pinL7 = 9;
int pinL8 = 10;
int pinL9 = 11;
int pinL10 = 12;

const String opencloseDoor =  "Labs/AI/door/openclose";
const String onoffLight1 =  "Labs/AI/Light/1";
const String onoffLight2 =  "Labs/AI/Light/2";
const String onoffLight3 =  "Labs/AI/Light/3";
const String onoffLight4 =  "Labs/AI/Light/4";
const String onoffLight5 =  "Labs/AI/Light/5";
const String onoffLight6 =  "Labs/AI/Light/6";
const String onoffLight7 =  "Labs/AI/Light/7";
const String onoffLight8 =  "Labs/AI/Light/8";
const String onoffLight9 =  "Labs/AI/Light/9";
const String onoffLight10 =  "Labs/AI/Light/10";

void connect() {
  Serial.print("connecting...");
  while (!client.connect("Relay1", "Labs", "password")) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");

  client.subscribe(opencloseDoor, 2);
  client.subscribe(onoffLight1, 2);
  client.subscribe(onoffLight2, 2);
  client.subscribe(onoffLight3, 2);
  client.subscribe(onoffLight4, 2);
  client.subscribe(onoffLight5, 2);
  client.subscribe(onoffLight6, 2);
  client.subscribe(onoffLight7, 2);
  client.subscribe(onoffLight8, 2);
  client.subscribe(onoffLight9, 2);
  client.subscribe(onoffLight10, 2);
}

void messageReceived(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
  if (topic == opencloseDoor) {
    if (payload == "open")  {
      digitalWrite(pinOCD, LOW);
      delay(1000);
      digitalWrite(pinOCD, HIGH);
    }
  } else {
    onoffLight(topic, payload);
  }

}

void onoffLight(String topic, String payload) {

  if (topic == onoffLight1) {
    onoffLightWorker(pinL1, payload);
  }
  else if (topic == onoffLight2) {
    onoffLightWorker(pinL2, payload);
  }
  else if (topic == onoffLight3) {
    onoffLightWorker(pinL3, payload);
  }
  else if (topic == onoffLight4) {
    onoffLightWorker(pinL4, payload);
  }
  else if (topic == onoffLight5) {
    onoffLightWorker(pinL5, payload);
  }
  else if (topic == onoffLight6) {
    onoffLightWorker(pinL6, payload);
  }
  else if (topic == onoffLight7) {
    onoffLightWorker(pinL7, payload);
  }
  else if (topic == onoffLight8) {
    onoffLightWorker(pinL8, payload);
  }
  else if (topic == onoffLight9) {
    onoffLightWorker(pinL9, payload);
  }
  else if (topic == onoffLight10) {
    onoffLightWorker(pinL10, payload);
  }
}

void onoffLightWorker(int pin, String payload) {
  if (payload == "on")  {
    digitalWrite(pin, LOW);
  } else if (payload == "off")  {
    digitalWrite(pin, HIGH);
  }
}

void setup() {
  Serial.begin(115200);
  Ethernet.begin(mac, ip);

  pinMode(pinOCD, OUTPUT);
  pinMode(pinL1, OUTPUT);
  pinMode(pinL2, OUTPUT);
  pinMode(pinL3, OUTPUT);
  pinMode(pinL4, OUTPUT);
  pinMode(pinL5, OUTPUT);
  pinMode(pinL6, OUTPUT);
  pinMode(pinL7, OUTPUT);
  pinMode(pinL8, OUTPUT);
  pinMode(pinL9, OUTPUT);
  pinMode(pinL10, OUTPUT);

  digitalWrite(pinOCD, HIGH);
  digitalWrite(pinL1, HIGH);
  digitalWrite(pinL2, HIGH);
  digitalWrite(pinL3, HIGH);
  digitalWrite(pinL4, HIGH);
  digitalWrite(pinL5, HIGH);
  digitalWrite(pinL6, HIGH);
  
  digitalWrite(pinL7, HIGH);
  digitalWrite(pinL8, HIGH);
  digitalWrite(pinL9, HIGH);
  digitalWrite(pinL10, HIGH);

  client.begin("192.168.1.7", 8883, net);
  client.onMessage(messageReceived);

  connect();
}

void loop() {
  client.loop();

  if (!client.connected()) {
    connect();
  }

  // publish a message roughly every second.
  //  if (millis() - lastMillis > 1000) {
  //    lastMillis = millis();
  //    client.publish("/hello", "world");
  //  }
}